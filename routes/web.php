<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/add', \App\Http\Livewire\Add::class)->name('add');
Route::get('/edit/{id}', \App\Http\Livewire\Edit::class)->name('edit');
Route::get('/view/{id}', \App\Http\Livewire\View::class)->name('view');
Route::get('/blog-view/{id}', \App\Http\Livewire\BlogView::class)->name('blog-view');
Route::get('/delete/{id}', \App\Http\Livewire\Delete::class)->name('delete');
Route::get('/published/{pageNumber?}', \App\Http\Livewire\Published::class)->name('published');
Route::get('/unpublished', \App\Http\Livewire\Unpublished::class)->name('unpublished');
Route::get('/future', \App\Http\Livewire\Future::class)->name('future');
Route::get('/admin', \App\Http\Livewire\Unpublished::class)->name('admin');

Route::get('/dashboard', function () {
  return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';

Route::get('/', \App\Http\Livewire\Blog::class)->name('blog-home');
Route::get('/blog/{pageNumber?}/{type?}', \App\Http\Livewire\Blog::class)->name('blog');
