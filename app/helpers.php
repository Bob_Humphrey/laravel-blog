<?php

function get_types()
{
  return [
    'A-1' => 'Permission Requests',
    'A10' => 'Articles',
    'A11' => 'Artworks',
    'A15' => 'Bible Quotes',
    'A20' => 'Books',
    'A25' => 'Concerts',
    'A28' => 'Drawings',
    'A30' => 'Essays',
    'A31' => 'Faces',
    'A32' => 'Great American Songbook',
    'A33' => 'Illustrations',
    'A35' => 'Images',
    'A37' => 'Magazine Covers',
    'A40' => 'Movies',
    'A46' => 'My Drawings',
    'A48' => 'My Photography',
    'A50' => 'Novels',
    'A52' => 'Other',
    'A54' => 'Paintings',
    'A56' => 'Photography',
    'A60' => 'Poems',
    'A65' => 'Prints',
    'A70' => 'Quotes',
    'A73' => 'Sculpture',
    'A75' => 'Series',
    'A80' => 'Songs',
    'A85' => 'Spirituality',
    'A90' => 'Stories',
    'A93' => 'Studio Ghibli',
    'A100' => 'TV',
    'A110' => 'Tweets',
  ];
}

function get_type_name($type)
{
  $types = get_types();
  if (substr($type, 0, 1) == 'A') {
    return $types[$type];
  }
  return $types['A' . $type];
}
