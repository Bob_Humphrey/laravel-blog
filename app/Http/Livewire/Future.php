<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Future extends Component
{
  use AuthorizesRequests;

  public $entries;
  public $futureEntries;
  public $startingPage;

  public function unpublish($id)
  {
    $this->authorize('admin');

    $entry = Entry::find($id);
    $entry->published = FALSE;
    $entry->published_date = NULL;
    $entry->save();
    session()->flash('success', "Entry has been unpublished: $entry->title");
    return redirect("/unpublished");
  }

  public function mount()
  {
    $tomorrow = Carbon::tomorrow()->format('Y-m-d');

    $this->futureEntries = Entry::select('id', 'title', 'author', 'image',  'created_at', 'published_date')->where('published', 1)->where('published_date', '>=', $tomorrow)->orderBy('published_date', 'DESC')->get();

    $this->entries = [];
  }

  public function render()
  {
    return view('livewire.list', ['list' => 'future']);
  }
}
