<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Livewire\WithFileUploads;

class View extends Component
{
  use WithFileUploads;

  public $entry;
  public $title;
  public $author;
  public $source;
  public $link;
  public $year;
  public $type;
  public $skip;
  public $body;
  public $image;
  public $embed;
  public $embed_on_small_screens;
  public $previousImage;
  public $attribution;
  public $published;
  public $published_date;
  public $publishedSwitchLabel;
  public $types;

  public function mount($id)
  {
    $entry = Entry::find($id);
    $this->entry = $entry;
    $this->title = $entry->title;
    $this->author = $entry->author;
    $this->source = $entry->source;
    $this->link = $entry->link;
    $this->year = $entry->year;
    $this->type = 'A' . $entry->type;
    $this->skip = $entry->skip;
    $this->body = $entry->body;
    $this->previousImage = $entry->image;
    $this->attribution = $entry->attribution;
    $this->embed = $entry->embed;
    $this->embed_on_small_screens = $entry->embed_on_small_screens;
    $this->published = $entry->published;
    if (is_null($entry->published_date)) {
      $this->published_date = NULL;
    } else {
      $this->published_date = substr($entry->published_date, 5, 5) . '-' . substr($entry->published_date, 0, 4);
    }
    $this->image = NULL;
    $this->types = get_types();
  }

  public function render()
  {
    return view('livewire.view');
  }
}
