<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use function PHPUnit\Framework\isNull;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Edit extends Component
{
  use WithFileUploads;
  use AuthorizesRequests;

  public $entry;
  public $title;
  public $author;
  public $source;
  public $link;
  public $year;
  public $type;
  public $skip;
  public $body;
  public $image;
  public $embed;
  public $embed_on_small_screens;
  public $previousImage;
  public $attribution;
  public $published;
  public $published_date;
  public $publishedSwitchLabel;
  public $types;

  protected $rules = [
    'title' => 'required|string|min:3|max:100',
    'author' => 'nullable|string|max:100',
    'source' => 'nullable|string|max:200',
    'link' => 'nullable|url|max:250',
    'year' => 'nullable|date_format:Y|min:4',
    'type' => 'required|string',
    'skip' => 'required|integer',
    'body' => 'nullable|string',
    'image' => 'nullable|image',
    'embed' => 'nullable|string',
    'embed_on_small_screens' => 'boolean',
    'attribution' => 'nullable|string',
    'published' => 'boolean',
    'published_date' => 'nullable|date_format:m-d-Y',
  ];

  public function edit()
  {
    $this->authorize('admin');

    $imageName = $this->previousImage;
    $formattedPublishedDate = NULL;

    $this->validate();

    if (!is_null($this->published_date)) {
      $formattedPublishedDate = substr($this->published_date, 6) . '-' . substr($this->published_date, 0, 2) . '-' . substr($this->published_date, 3, 2);
    }

    if (!is_null($this->image)) {
      $interventionImage = Image::make($this->image->path());
      $imageName = Str::random(40) . '.' . $this->image->extension();
      $normalSizePath = storage_path() . '/app/public/postimages/';
      $thumbnailPath = storage_path() . '/app/public/thumbnails/';
      // Normal size
      $interventionImage->resize(500, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $interventionImage->save($normalSizePath . $imageName);
      // Thumbnail
      $interventionImage->resize(100, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $interventionImage->save($thumbnailPath . $imageName);
    }

    $entry = $this->entry;
    $entry->title = Str::title($this->title);
    $entry->author = $this->author;
    $entry->source = $this->source;
    $entry->link = $this->link;
    $entry->year = $this->year;
    $entry->type = substr($this->type, 1);
    $entry->skip = $this->skip;
    $entry->body = $this->body;
    $entry->image = $imageName;
    $entry->attribution = $this->attribution;
    $entry->embed = $this->embed;
    $entry->embed_on_small_screens = $this->embed_on_small_screens;
    $entry->published = $this->published;
    $entry->published_date = $formattedPublishedDate;
    $entry->save();

    session()->flash('success', "Entry has been updated: $this->title");
    return redirect(route('blog-view', $entry->id));
  }

  public function mount($id)
  {
    $entry = Entry::find($id);
    $this->entry = $entry;
    $this->title = $this->trimIfString($entry->title);
    $this->author = $this->trimIfString($entry->author);
    $this->source = $this->trimIfString($entry->source);
    $this->link = $this->trimIfString($entry->link);
    $this->year = $this->trimIfString($entry->year);
    $this->type = 'A' . $entry->type;
    $this->skip = $entry->skip;
    $this->body = $this->trimIfString($entry->body);
    $this->previousImage = $entry->image;
    $this->embed = $this->trimIfString($entry->embed);
    $this->embed_on_small_screens = $entry->embed_on_small_screens;
    $this->attribution = $this->trimIfString($entry->attribution);
    $this->published = $entry->published;
    if (is_null($entry->published_date)) {
      $this->published_date = NULL;
    } else {
      $this->published_date = substr($entry->published_date, 5, 5) . '-' . substr($entry->published_date, 0, 4);
    }
    $this->types = get_types();
    $this->image = NULL;
  }

  public function render()
  {
    return view('livewire.edit');
  }

  private function trimIfString($value)
  {
    return is_string($value) ? trim($value) : $value;
  }
}
