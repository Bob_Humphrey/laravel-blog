<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Log;

class Pagination extends Component
{
  public $currentPageNumber;
  public $pages;
  public $pageLinks;
  public $prevActive;
  public $nextActive;
  public $goToPage;
  public $redirect;
  public $type;
  public $types;

  public function changePage()
  {
    if (substr($this->type, 0, 1) == 'A') {
      return redirect()->to('/blog/' . $this->goToPage . '/' . $this->type);
    }
    return redirect()->to('/blog/' . $this->goToPage . '/A' . $this->type);
  }

  public function updatedType($type)
  {
    return redirect()->to('/blog/' . '1/' . $type);
  }

  public function mount($type, $totalEntriesCount, $currentPageNumber, $entriesPerPage, $numberPageLinksDisplayed, $redirect)
  {
    $this->pages = ceil($totalEntriesCount / $entriesPerPage);
    $currentPageNumber = is_numeric($currentPageNumber) ? $currentPageNumber : 1;
    $currentPageNumber = $currentPageNumber < 1 ? 1 : $currentPageNumber;
    $currentPageNumber = $currentPageNumber > $this->pages ? $this->pages : $currentPageNumber;
    $this->page = $currentPageNumber;
    $queryOffset = ($currentPageNumber - 1)  * $entriesPerPage;

    // Which page links should be displayed
    $pageLinks = collect([]);

    // If number of pages <= total links displayed
    if ($this->pages <= $numberPageLinksDisplayed) {
      for ($i = 1; $i <= $this->pages; $i++) {
        $pageLinks->push($i);
      }
    }

    // If number of pages > total links displayed
    if (($this->pages > $numberPageLinksDisplayed) && ($currentPageNumber <= ($this->pages - (($numberPageLinksDisplayed - 1) / 2)))) {
      $firstPage = $currentPageNumber - (($numberPageLinksDisplayed - 1) / 2);
      $firstPage = $firstPage < 1 ? 1 : $firstPage;
      for ($i = $firstPage; $i <= $firstPage + $numberPageLinksDisplayed - 1; $i++) {
        if ($i > $this->pages) {
          break;
        }
        $pageLinks->push($i);
      }
    }
    if (($this->pages > $numberPageLinksDisplayed) && ($currentPageNumber > ($this->pages - (($numberPageLinksDisplayed - 1) / 2)))) {
      for ($i = $this->pages - $numberPageLinksDisplayed + 1; $i <= $this->pages; $i++) {
        if ($i > $this->pages) {
          break;
        }
        $pageLinks->push($i);
      }
    }

    $this->pageLinks = $pageLinks->all();
    $this->prevActive = $currentPageNumber > 1 ? TRUE : FALSE;
    $this->nextActive = $currentPageNumber < $this->pages ? TRUE : FALSE;
    $this->redirect = $redirect;
    if (substr($type, 0, 1) == 'A') {
      $this->type = $type;
    } else {
      $this->type = 'A' . $type;
    }

    $this->types = get_types();
  }

  public function render()
  {
    return view('livewire.pagination');
  }
}
