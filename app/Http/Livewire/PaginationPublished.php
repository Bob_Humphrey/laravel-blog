<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Log;

class PaginationPublished extends Component
{
  public $currentPageNumber;
  public $pages;
  public $pageLinks;
  public $prevActive;
  public $nextActive;
  public $goToPage;
  public $redirect;

  public function changePage()
  {
    return redirect()->to('/published/' . $this->goToPage);
  }

  public function mount($totalEntriesCount, $currentPageNumber, $entriesPerPage, $numberPageLinksDisplayed, $redirect)
  {
    $this->pages = ceil($totalEntriesCount / $entriesPerPage);
    $currentPageNumber = is_numeric($currentPageNumber) ? $currentPageNumber : 1;
    $currentPageNumber = $currentPageNumber < 1 ? 1 : $currentPageNumber;
    $currentPageNumber = $currentPageNumber > $this->pages ? $this->pages : $currentPageNumber;
    $this->page = $currentPageNumber;

    // Which page links should be displayed
    $pageLinks = collect([]);

    // If number of pages <= total links displayed
    if ($this->pages <= $numberPageLinksDisplayed) {
      for ($i = 1; $i <= $this->pages; $i++) {
        $pageLinks->push($i);
      }
    }

    // If number of pages > total links displayed
    if (($this->pages > $numberPageLinksDisplayed) && ($currentPageNumber <= ($this->pages - (($numberPageLinksDisplayed - 1) / 2)))) {
      $firstPage = $currentPageNumber - (($numberPageLinksDisplayed - 1) / 2);
      $firstPage = $firstPage < 1 ? 1 : $firstPage;
      for ($i = $firstPage; $i <= $firstPage + $numberPageLinksDisplayed - 1; $i++) {
        if ($i > $this->pages) {
          break;
        }
        $pageLinks->push($i);
      }
    }
    if (($this->pages > $numberPageLinksDisplayed) && ($currentPageNumber > ($this->pages - (($numberPageLinksDisplayed - 1) / 2)))) {
      for ($i = $this->pages - $numberPageLinksDisplayed + 1; $i <= $this->pages; $i++) {
        if ($i > $this->pages) {
          break;
        }
        $pageLinks->push($i);
      }
    }

    $this->pageLinks = $pageLinks->all();
    $this->prevActive = $currentPageNumber > 1 ? TRUE : FALSE;
    $this->nextActive = $currentPageNumber < $this->pages ? TRUE : FALSE;
    $this->redirect = $redirect;
  }

  public function render()
  {
    return view('livewire.pagination-published');
  }
}
