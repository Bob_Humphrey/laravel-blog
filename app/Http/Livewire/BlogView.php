<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class BlogView extends Component
{
  use AuthorizesRequests;

  public $entry;
  public $title;
  public $author;
  public $source;
  public $link;
  public $year;
  public $type;
  public $skip;
  public $body;
  public $image;
  public $embed;
  public $previousImage;
  public $attribution;
  public $published;
  public $published_date;
  public $publishedSwitchLabel;

  protected $rules = [
    'skip' => 'required|integer',
  ];

  public function updateSkip()
  {
    $this->authorize('admin');
    $entry = $this->entry;
    $entry->skip = $this->skip;
    $entry->save();
    session()->flash('success', "Entry has been updated: $this->title");
    $redirect = '/blog-view' . '/' . $this->entry->id;
    return redirect($redirect);
  }

  public function mount($id)
  {
    $entry = Entry::find($id);
    $this->entry = $entry;
    $this->title = $entry->title;
    $this->author = $entry->author;
    $this->source = $entry->source;
    $this->link = $entry->link;
    $this->year = $entry->year;
    $this->type = $entry->type;
    $this->skip = $entry->skip;
    $this->body = $entry->body;
    $this->previousImage = $entry->image;
    $this->attribution = $entry->attribution;
    $this->embed = $entry->embed;
    $this->published = $entry->published;
    if (is_null($entry->published_date)) {
      $this->published_date = NULL;
    } else {
      $this->published_date = substr($entry->published_date, 5, 5) . '-' . substr($entry->published_date, 0, 4);
    }
    $this->image = NULL;
  }

  public function render()
  {
    return view('livewire.blog-view');
  }
}
