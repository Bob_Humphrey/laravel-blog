<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Published extends Component
{
  use AuthorizesRequests;

  public $entries;
  public $futureEntries;
  public $startingPage;
  public $paginationTotalEntriesCount;
  public $paginationCurrentPageNumber;
  public $paginationEntriesPerPage;
  public $paginationNumberPageLinksDisplayed;
  public $paginationRedirect;

  public function unpublish($id)
  {
    $this->authorize('admin');

    $entry = Entry::find($id);
    $entry->published = FALSE;
    $entry->published_date = NULL;
    $entry->save();
    session()->flash('success', "Entry has been unpublished: $entry->title");
    return redirect("/unpublished");
  }

  public function mount($pageNumber = 1)
  {
    $tomorrow = Carbon::tomorrow()->format('Y-m-d');

    $this->paginationEntriesPerPage = 100;
    $this->paginationNumberPageLinksDisplayed = 7;
    $this->paginationRedirect = 'published/';
    $this->paginationTotalEntriesCount = Entry::where('published', 1)->where('published_date', '<', $tomorrow)->count();
    $totalPageCount = ceil($this->paginationTotalEntriesCount / $this->paginationEntriesPerPage);
    $pageNumber = $pageNumber > $totalPageCount ? $totalPageCount : $pageNumber;
    $pageNumber = $pageNumber < 1 ? 1 : $pageNumber;
    $this->paginationCurrentPageNumber = $pageNumber;
    $this->startingPage = ($pageNumber - 1) * 5 + 1;

    $this->entries = Entry::select('id', 'title', 'author', 'image',  'created_at', 'published_date')->where('published', 1)->where('published_date', '<', $tomorrow)->orderBy('published_date', 'DESC')->forPage($pageNumber, $this->paginationEntriesPerPage)->get();

    $this->futureEntries = [];
  }

  public function render()
  {
    return view('livewire.list', ['list' => 'published']);
  }
}
