<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Unpublished extends Component
{
  use AuthorizesRequests;

  public $entries;
  public $futureEntries = [];
  public $startingPage;

  public function publish($id)
  {
    $this->authorize('admin');

    $entry = Entry::find($id);
    $entry->published = TRUE;
    $entry->published_date = Carbon::today()->format('Y-m-d');
    $entry->save();
    session()->flash('success', "Entry has been published: $entry->title");
    return redirect("/published");
  }

  public function mount()
  {
    $this->entries = Entry::select('id', 'type', 'title', 'author', 'image', 'created_at', 'published_date')->where('published', 0)->orderBy('type')->orderBy('id')->get();
  }

  public function render()
  {
    return view('livewire.list', ['list' => 'unpublished']);
  }
}
