<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Add extends Component
{
  use WithFileUploads;
  use AuthorizesRequests;

  public $types;
  public $title;
  public $author;
  public $source;
  public $link;
  public $year;
  public $type;
  public $skip;
  public $body;
  public $image;
  public $embed;
  public $embed_on_small_screens;
  public $previousImage;
  public $attribution;
  public $published;
  public $published_date;

  protected $rules = [
    'title' => 'required|string|min:3|max:100',
    'author' => 'nullable|string|max:100',
    'source' => 'nullable|string|max:200',
    'link' => 'nullable|url|max:250',
    'year' => 'nullable|date_format:Y|min:4',
    'type' => 'required|string',
    'skip' => 'required|integer',
    'body' => 'nullable|string',
    'image' => 'nullable|image',
    'embed' => 'nullable|string',
    'attribution' => 'nullable|string',
    'published' => 'boolean',
    'published_date' => 'nullable|date_format:m-d-Y',
    'embed_on_small_screens' => 'boolean',
  ];

  public function add()
  {
    $this->authorize('admin');

    $imageName = NULL;
    $formattedPublishedDate = NULL;

    $this->validate();

    if (!is_null($this->published_date)) {
      $formattedPublishedDate = substr($this->published_date, 6) . '-' . substr($this->published_date, 0, 2) . '-' . substr($this->published_date, 3, 2);
    }

    if (!is_null($this->image)) {
      $interventionImage = Image::make($this->image->path());
      $imageName = Str::random(40) . '.' . $this->image->extension();
      $normalSizePath = storage_path() . '/app/public/postimages/';
      $thumbnailPath = storage_path() . '/app/public/thumbnails/';
      // Normal size
      $interventionImage->resize(500, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $interventionImage->save($normalSizePath . $imageName);
      // Thumbnail
      $interventionImage->resize(100, null, function ($constraint) {
        $constraint->aspectRatio();
      });
      $interventionImage->save($thumbnailPath . $imageName);
    }

    $entry = Entry::create([
      'author' => $this->trimIfString($this->author),
      'source' => $this->trimIfString($this->source),
      'link' => $this->trimIfString($this->link),
      'year' => $this->trimIfString($this->year),
      'type' => substr($this->type, 1),
      'skip' => $this->skip,
      'body' => $this->trimIfString($this->body),
      'image' => $imageName,
      'embed' => $this->trimIfString($this->embed),
      'embed_on_small_screens' => $this->embed_on_small_screens,
      'attribution' => $this->trimIfString($this->attribution),
      'published' => $this->published,
      'published_date' => $formattedPublishedDate,
      'title' => $this->trimIfString(Str::title($this->title)),
    ]);

    $id = $entry->id;

    session()->flash('success', "New entry has been added: $this->title");
    return redirect(route('blog-view', $id));
  }

  public function mount()
  {
    $this->published = FALSE;
    $this->body = '';
    $this->image = NULL;
    $this->embed = NULL;
    $this->embed_on_small_screens = true;
    $this->attribution = NULL;
    $this->previousImage = NULL;
    $this->skip = 0;
    $this->publishedSwitchLabel = 'Not Published';
    $this->types = get_types();
  }


  public function render()
  {
    return view(
      'livewire.add',
      [
        // 'title' => $this->title,
        // 'author' => $this->author,
        // 'source' => $this->source,
        // 'link' => $this->link,
        // 'year' => $this->year,
        // 'type' => $this->type,
        // 'skip' => $this->skip,
        // 'body' => $this->body,
        // 'image' => $this->image,
        // 'embed' => $this->embed,
        // 'attribution' => $this->attribution,
        // 'published' => $this->published,
        // 'published_date' => $this->published_date,
        //'types' => $this->types,
      ]
    );
  }

  private function trimIfString($value)
  {
    return is_string($value) ? trim($value) : $value;
  }
}
