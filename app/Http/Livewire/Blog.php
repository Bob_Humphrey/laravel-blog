<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class Blog extends Component
{
  public $entries;
  public $paginationTotalEntriesCount;
  public $paginationCurrentPageNumber;
  public $paginationEntriesPerPage;
  public $paginationNumberPageLinksDisplayed;
  public $paginationRedirect;
  public $type;
  public $types;

  public function mount($pageNumber = 1, $type = 'A0')
  {
    $this->type = substr($type, 1);
    $this->paginationEntriesPerPage = 20;
    $this->paginationNumberPageLinksDisplayed = 7;
    $this->paginationRedirect = 'blog/';
    $today = Carbon::today()->format('Y-m-d');

    // Pages
    if ($this->type == '0') {
      $this->paginationTotalEntriesCount = Entry::where('published', 1)->where('published_date', '<=', $today)->orderBy('published_date', 'DESC')->count();
    } else {
      $this->paginationTotalEntriesCount = Entry::where('published', 1)->where('published_date', '<=', $today)->where('type', $this->type)->orderBy('published_date', 'DESC')->count();
    }
    $totalPageCount = ceil($this->paginationTotalEntriesCount / $this->paginationEntriesPerPage);
    $pageNumber = $pageNumber > $totalPageCount ? $totalPageCount : $pageNumber;
    $pageNumber = $pageNumber < 1 ? 1 : $pageNumber;

    // Entries
    if ($this->type == '0') {
      $this->entries = Entry::where('published', 1)->where('published_date', '<=', $today)->orderBy('published_date', 'DESC')->forPage($pageNumber, $this->paginationEntriesPerPage)->get();
    } else {
      $this->entries = Entry::where('published', 1)->where('published_date', '<=', $today)->where('type', $this->type)->orderBy('published_date', 'DESC')->forPage($pageNumber, $this->paginationEntriesPerPage)->get();
    }
    $this->entries = $this->entries->map(function ($entry) {
      $entry->formattedPubishedDate = Carbon::createFromDate($entry->published_date)->format('F j, Y');
      return $entry;
    });

    $this->paginationCurrentPageNumber = $pageNumber;
    $this->types = get_types();
  }

  public function render()
  {
    return view('livewire.blog');
  }
}
