<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Log;

class TypeFilter extends Component
{
  public $types;
  public $type;

  public function changePage()
  {
    $type = substr($this->type, 1);
    return redirect('blog/1/' . $type);
  }

  public function mount()
  {
    $this->types = get_types();
  }

  public function render()
  {
    return view('livewire.type-filter');
  }
}
