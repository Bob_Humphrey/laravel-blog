<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Delete extends Component
{
  use WithFileUploads;
  use AuthorizesRequests;

  public $entry;
  public $title;
  public $author;
  public $source;
  public $link;
  public $year;
  public $type;
  public $skip;
  public $body;
  public $image;
  public $previousImage;
  public $attribution;
  public $published;
  public $published_date;
  public $publishedSwitchLabel;
  public $types;
  public $embed_on_small_screens;

  public function delete()
  {
    $this->authorize('admin');

    $this->entry->delete();
    session()->flash('success', "Entry has been deleted: $this->title");
    return redirect("/published");
  }

  public function mount($id)
  {
    $entry = Entry::find($id);
    $this->entry = $entry;
    $this->title = $entry->title;
    $this->author = $entry->author;
    $this->source = $entry->source;
    $this->link = $entry->link;
    $this->year = $entry->year;
    $this->type = 'A' . $entry->type;
    $this->skip = $entry->skip;
    $this->body = $entry->body;
    $this->previousImage = $entry->image;
    $this->embed = $entry->embed;
    $this->embed_on_small_screens = $entry->embed_on_small_screens;
    $this->attribution = $entry->attribution;
    $this->published = $entry->published;
    $this->published_date = $entry->published_date;
    $this->image = NULL;
    $this->types = get_types();
  }

  public function render()
  {
    return view('livewire.delete');
  }
}
