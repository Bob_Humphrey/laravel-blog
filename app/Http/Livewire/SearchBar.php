<?php

namespace App\Http\Livewire;

use App\Models\Entry;
use Livewire\Component;

class SearchBar extends Component
{
  public $query;
  public $entries;
  public $highlightIndex;

  public function mount()
  {
    $this->resetState();
  }

  public function resetState()
  {
    $this->query = '';
    $this->entries = [];
    $this->highlightIndex = 0;
  }

  public function incrementHighlight()
  {
    if ($this->highlightIndex === count($this->contacts) - 1) {
      $this->highlightIndex = 0;
      return;
    }
    $this->highlightIndex++;
  }

  public function decrementHighlight()
  {
    if ($this->highlightIndex === 0) {
      $this->highlightIndex = count($this->contacts) - 1;
      return;
    }
    $this->highlightIndex--;
  }

  public function selectEntry()
  {
    $entry = $this->entries[$this->highlightIndex] ?? null;
    if ($entry) {
      $this->redirect(route('blog-view', $entry['id']));
    }
  }

  public function updatedQuery()
  {
    $this->entries = Entry::where('title', 'like', '%' . $this->query . '%')
      ->get()
      ->toArray();
  }
  public function render()
  {
    return view('livewire.search-bar');
  }
}
