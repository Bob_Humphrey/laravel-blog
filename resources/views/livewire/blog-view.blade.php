<div class="sm:w-10/12 md:w-7/12 lg:w-blog mx-auto ">

  @include('shared.blog-view', ['source' => 'backend'])

  {{-- LINKS TO OTHER ACTIONS --}}

  <div class="flex flex-col lg:flex-row items-center justify-center w-full">
    <x-label for="skip" class="w-12 mx-4" />
    <x-input name="skip" wire:model="skip" class="w-24 p-2 border border-gray-400 rounded mx-4" />
    <button type="submit" wire:click="updateSkip" class="button lg:mx-4">
      Update Skip
      </a>
    </button>
    <a href={{ url('edit/' . $entry->id) }} class="button lg:mx-4">
      Edit
    </a>
    <a href={{ url('delete/' . $entry->id) }} class="button lg:mx-4">
      Delete
    </a>
  </div>

</div>
