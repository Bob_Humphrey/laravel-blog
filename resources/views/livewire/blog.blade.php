@php

@endphp


<div class="sm:w-10/12 md:w-7/12 lg:w-blog mx-auto ">

  <div>
    <div>
      @livewire('pagination', [
      'type' => $type,
      'totalEntriesCount' => $paginationTotalEntriesCount,
      'currentPageNumber' => $paginationCurrentPageNumber,
      'entriesPerPage' => $paginationEntriesPerPage,
      'numberPageLinksDisplayed' => $paginationNumberPageLinksDisplayed,
      'redirect'=> $paginationRedirect
      ])
    </div>
  </div>

  @if (count($this->entries) > 0)

    @foreach ($this->entries as $entry)

      @include('shared.blog-view', ['source' => 'frontend'])

    @endforeach

  @else

    <div class="mb-12 text-3xl">
      There are no posts for this type.
    </div>

  @endif

  <div class="mb-16">
    @livewire('pagination', [
    'type' => $type,
    'totalEntriesCount' => $paginationTotalEntriesCount,
    'currentPageNumber' => $paginationCurrentPageNumber,
    'entriesPerPage' => $paginationEntriesPerPage,
    'numberPageLinksDisplayed' => $paginationNumberPageLinksDisplayed,
    'redirect'=> $paginationRedirect
    ])
  </div>


</div>
