<div class="hidden lg:flex text-sm items-center space-x-4">
  <select name="type" id="type" wire:model="type" class="p-2 rounded border border-gray-400 w-full appearance-none">
    <option value="">Pick a type...</option>
    @foreach ($types as $key => $value)
      <option value=A{{ $key }}>{{ $value }}</option>
    @endforeach
  </select>
  <button class="rounded-full cursor-pointer px-2 border border-gray-400-rounded hover:bg-gray-600 hover:text-white"
    wire:click="changePage">
    Go
  </button>
</div>
