@php
$previousType = '';
$currentPage = $startingPage;
@endphp


<div class="w-11/12 xl:w-10/12 bg-white p-6 border border-gray-200 mx-auto mb-10">

  <h2 class="w-full heading text-center pt-4 pb-10">
    @if ($list === 'future')
      Future
    @elseif ($list === 'unpublished')
      Unpublished
    @else
      Published
    @endif
  </h2>

  @if ($list === 'published')
    <div class="w-3/5 mx-auto">
      @livewire('pagination-published', [
      'totalEntriesCount' => $paginationTotalEntriesCount,
      'currentPageNumber' => $paginationCurrentPageNumber,
      'entriesPerPage' => $paginationEntriesPerPage,
      'numberPageLinksDisplayed' => $paginationNumberPageLinksDisplayed,
      'redirect'=> $paginationRedirect
      ])
    </div>
  @endif

  <table class="w-11/12 mx-auto">
    <tr class="grid grid-cols-12 gap-x-12 font-nunito_bold text-sm border-b border-gray-300">
      <th class="hidden lg:block lg:col-span-1 text-left py-3">
        @if ($list === 'unpublished')
          Draft
        @else
          Published
        @endif
      </th>
      <th class="hidden lg:block lg:col-span-1 text-left py-3">Image</th>
      <th class="col-span-7 lg:col-span-4 text-left py-3">Title</th>
      <th class="hidden lg:block lg:col-span-3 text-left py-3">Author</th>
      <th class="col-span-5 lg:col-span-3 text-center py-3">Actions</th>
    </tr>

    {{-- FUTURE ENTRIES --}}

    @foreach ($this->futureEntries as $entry)

      @if ($loop->index === 0)
        <tr class="font-roboto bg-gray-50 border-b border-r border-l border-gray-300">
          <th class="w-full text-center py-3">Future</th>
        </tr>
      @endif

      @php
        $publishedDate = substr($entry->published_date, 5, 5) . '-' . substr($entry->created_at, 0, 4);
      @endphp

      <tr
        class="grid grid-cols-12 gap-x-12 text-sm font-nunito_light {{ $loop->last ? '' : 'border-b border-gray-300' }}">
        <td class="hidden lg:flex lg:col-span-1 items-center text-left py-3">
          {{ $publishedDate }}
        </td>
        <td class="hidden lg:flex lg:col-span-1 items-center text-left py-3">
          @if ($entry->image)
            <img src={{ asset('storage/postimages/' . $entry->image) }} />
          @endif
        </td>
        <td class="col-span-7 lg:col-span-4 flex items-center text-left py-3">
          {{ $entry->title }}
        </td>
        <td class="hidden lg:flex lg:col-span-3 items-center text-left py-3">
          {{ $entry->author }}
        </td>
        <td class="col-span-5 lg:col-span-3 flex items-center justify-around text-left py-3">
          <div class="tooltip">
            <span class="tooltip-text">Blog View</span>
            <a href={{ url('blog-view/' . $entry->id) }}>
              <x-heroicon-s-eye class="w-7 link" />
            </a>
          </div>
          <div class="tooltip">
            <span class="tooltip-text">View</span>
            <a href={{ url('view/' . $entry->id) }}>
              <x-heroicon-o-eye class="w-7 link" />
            </a>
          </div>
          @if ($list === 'unpublished')
            <div class="tooltip">
              <span class="tooltip-text">Publish</span>
              <div wire:click="publish({{ $entry->id }})">
                <x-zondicon-add-outline class="w-5 link" />
              </div>
            </div>
          @else
            <div class="tooltip">
              <span class="tooltip-text">Unpublish</span>
              <div wire:click="unpublish({{ $entry->id }})">
                <x-zondicon-minus-outline class="w-5 link" />
              </div>
            </div>
          @endif
          <div class="tooltip">
            <span class="tooltip-text">Edit</span>
            <a href={{ url('edit/' . $entry->id) }}>
              <x-zondicon-edit-pencil class="w-5 link" />
            </a>
          </div>
          <div class="tooltip">
            <span class="tooltip-text">Delete</span>
            <a href={{ url('delete/' . $entry->id) }}>
              <x-zondicon-close-outline class="w-5 link" />
            </a>
          </div>
        </td>
      </tr>
    @endforeach

    {{-- PREVIOUS ENTRIES --}}

    @foreach ($this->entries as $entry)

      @if ($list === 'published' && $loop->index % 20 === 0)
        <tr class="font-roboto bg-gray-50 border-b border-r border-l border-gray-300">
          <th class="w-full text-center py-3">Page {{ $currentPage }}</th>
        </tr>
        @php
          $currentPage++;
        @endphp
      @endif

      @if ($list === 'unpublished')
        @if ($entry->type != $previousType)
          @php
            $typeName = get_type_name($entry->type);
            $previousType = $entry->type;
          @endphp
          <tr class="font-roboto border-gray-300 bg-gray-50 border-b border-r border-l">
            <th class="py-3">{{ $typeName }}</th>
          </tr>
        @endif
      @endif

      @php
        $draftDate = substr($entry->created_at, 5, 5) . '-' . substr($entry->created_at, 0, 4);
        $publishedDate = substr($entry->published_date, 5, 5) . '-' . substr($entry->created_at, 0, 4);
      @endphp
      <tr
        class="grid grid-cols-12 gap-x-12 text-sm font-nunito_light {{ $loop->last ? '' : 'border-b border-gray-300' }}">
        <td class="hidden lg:flex lg:col-span-1 items-center text-left py-3">
          @if ($list === 'unpublished')
            {{ $draftDate }}
          @else
            {{ $publishedDate }}
          @endif
        </td>
        <td class="hidden lg:flex lg:col-span-1 items-center text-left py-3">
          @if ($entry->image)
            <img src={{ asset('storage/postimages/' . $entry->image) }} />
          @endif
        </td>
        <td class="col-span-7 lg:col-span-4 flex items-center text-left py-3">
          {{ $entry->title }}
        </td>
        <td class="hidden lg:flex lg:col-span-3 items-center text-left py-3">
          {{ $entry->author }}
        </td>
        <td class="col-span-5 lg:col-span-3 flex items-center justify-around text-left py-3">
          <div class="tooltip">
            <span class="tooltip-text">Blog View</span>
            <a href={{ url('blog-view/' . $entry->id) }}>
              <x-heroicon-s-eye class="w-7 link" />
            </a>
          </div>
          <div class="tooltip">
            <span class="tooltip-text">View</span>
            <a href={{ url('view/' . $entry->id) }}>
              <x-heroicon-o-eye class="w-7 link" />
            </a>
          </div>
          @if ($list === 'unpublished')
            <div class="tooltip">
              <span class="tooltip-text">Publish</span>
              <div wire:click="publish({{ $entry->id }})">
                <x-zondicon-add-outline class="w-5 link" />
              </div>
            </div>
          @else
            <div class="tooltip">
              <span class="tooltip-text">Unpublish</span>
              <div wire:click="unpublish({{ $entry->id }})">
                <x-zondicon-minus-outline class="w-5 link" />
              </div>
            </div>
          @endif
          <div class="tooltip">
            <span class="tooltip-text">Edit</span>
            <a href={{ url('edit/' . $entry->id) }}>
              <x-zondicon-edit-pencil class="w-5 link" />
            </a>
          </div>
          <div class="tooltip">
            <span class="tooltip-text">Delete</span>
            <a href={{ url('delete/' . $entry->id) }}>
              <x-zondicon-close-outline class="w-5 link" />
            </a>
          </div>

        </td>
      </tr>
    @endforeach
  </table>

  @if ($list === 'published')
    <div class="w-3/5 mx-auto mt-8">
      @livewire('pagination-published', [
      'totalEntriesCount' => $paginationTotalEntriesCount,
      'currentPageNumber' => $paginationCurrentPageNumber,
      'entriesPerPage' => $paginationEntriesPerPage,
      'numberPageLinksDisplayed' => $paginationNumberPageLinksDisplayed,
      'redirect'=> $paginationRedirect
      ])
    </div>
  @endif

</div>
