{{-- @if ($pages > 1) --}}
<div class="border border border-gray-200 mb-12 p-6">
  <div class="flex flex-col lg:flex-row items-center justify-center lg:justify-between text-sm p-2">

    {{-- POST TYPE --}}

    <select name="type" id="type" wire:model="type"
      class="p-2 rounded border border-gray-400 w-48 appearance-none mb-3 lg:mb-0">
      <option value="A0">All</option>
      @foreach ($types as $key => $value)
        <option value={{ $key }}>{{ $value }}</option>
      @endforeach
    </select>

    {{-- PAGE LINKS --}}

    @if ($pages > 1)
      <div class=" flex divide-gray-400 divide-x border border-gray-400 rounded">
        @if ($currentPageNumber == 1)
          <div class="px-3 py-2 text-gray-300">
            <x-heroicon-s-chevron-left class="w-5" />
          </div>
        @else
          <div>
            <a href="{{ url('blog/' . $currentPageNumber - 1 . '/' . $type) }}"
              class="relative inline-flex items-center px-3 py-2 cursor-pointer">
              <x-heroicon-s-chevron-left class="w-5" />
            </a>
          </div>
        @endif
        @foreach ($pageLinks as $pageLink)
          @if ($pageLink == $currentPageNumber)
            <div class="bg-gray-600 text-white px-3 py-2">
              {{ $pageLink }}
            </div>
          @else
            <div class="hidden md:block">
              <a href="{{ url('blog/' . $pageLink . '/' . $type) }}"
                class="relative inline-flex items-center px-3 py-2 cursor-pointer">
                {{ $pageLink }}
              </a>
            </div>
          @endif
        @endforeach
        @if ($currentPageNumber == $pages)
          <div class="px-3 py-2 text-gray-300">
            <x-heroicon-s-chevron-right class="w-5" />
          </div>
        @else
          <div>
            <a href="{{ url('blog/' . $currentPageNumber + 1 . '/' . $type) }}"
              class="relative inline-flex items-center px-3 py-2 cursor-pointer">
              <x-heroicon-s-chevron-right class="w-5" />
            </a>
          </div>
        @endif
      </div>
    @else
      <div></div>
    @endif

    {{-- GO TO PAGE FORM --}}

    @if ($pages > 1)
      <div class="hidden lg:flex items-center space-x-4">
        <div>
          Go to Page 1 - {{ $pages }}
        </div>
        <x-input name="goToPage" wire:model="goToPage" class="bg-gray-200 p-2 w-10 border border-gray-400 rounded" />
        <button
          class="rounded-full cursor-pointer px-2 border border-gray-400-rounded hover:bg-gray-600 hover:text-white"
          wire:click="changePage">
          Go
        </button>
      </div>
    @else
      <div></div>
    @endif

  </div>
</div>
