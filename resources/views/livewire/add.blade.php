<div class="w-11/12 lg:w-admin-lg xl:w-admin-xl md:grid mx-auto">

  <h2 class="w-full heading text-center pb-6">
    Add Entry
  </h2>

  @include('livewire.form', ['disabled' => '', 'showImage' => FALSE])

  {{-- SUBMIT BUTTON --}}

  <div class="flex justify-center pt-3">
    <button type="submit" wire:click="add" class="button mx-auto">
      Add
    </button>
  </div>

</div>
