<div class="w-11/12 lg:w-admin-lg xl:w-admin-xl lg:grid grid-cols-12 gap-x-12 mx-auto">

  {{-- FIRST COLUMN --}}

  <div class="col-span-4">

    {{-- PUBLISHED --}}

    <div class="pb-3">
      <div class="relative inline-block w-12 mr-2 align-middle select-none transition duration-200 ease-in">
        <input type="checkbox" name="published" id="published" wire:model="published"
          class="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 border-red-600 appearance-none cursor-pointer" />
        <label for="published"
          class="toggle-label block overflow-hidden h-6 rounded-full bg-red-600 cursor-pointer"></label>
      </div>
      <label for="published" class="{{ $published ? 'text-green-600' : 'text-red-600' }} uppercase pr-4">
        {{ $published ? 'Published' : 'Not Published' }}
      </label>
    </div>

    {{-- PUBLISHED DATE --}}

    <x-input-group name="published_date">
      @if ($disabled)
        <x-datepicker name="published_date" id="published_date" wire:model="published_date"
          class="p-2 rounded border border-gray-400 w-full appearance-none" placeholder="MM-DD-YYYY" disabled />
      @else
        <x-datepicker name="published_date" id="published_date" wire:model="published_date"
          class="p-2 rounded border border-gray-400 w-full appearance-none" placeholder="MM-DD-YYYY" />
      @endif
    </x-input-group>

    {{-- IMAGE TABS --}}

    <div x-data="{
      openTab: 1,
      activeClasses: 'border-l border-t border-r rounded-t border-gray-400',
      inactiveClasses: 'hover:text-tan'
    }" class="">
      <ul class="flex border-b border-gray-400 text-sm text-gray-700">
        <li @click="openTab = 1" :class="{ '-mb-px': openTab === 1 }" class="-mb-px mr-1">
          <a :class="openTab === 1 ? activeClasses : inactiveClasses" class="bg-white inline-block py-2 px-4" href=" #">
            IMAGE FILE
          </a>
        </li>
        <li @click="openTab = 2" :class="{ '-mb-px': openTab === 2 }" class="mr-1">
          <a :class="openTab === 2 ? activeClasses : inactiveClasses" class="bg-white inline-block py-2 px-4" href="#">
            EMBEDDED CONTENT
          </a>
        </li>
      </ul>
      <div class="w-full pt-4">

        {{-- TAB 1 --}}

        <div x-show="openTab === 1">

          {{-- IMAGE UPLOAD --}}

          <x-input-group name="image">
            @if ($disabled)
              <x-input type="file" name="image" wire:model="image"
                class="p-2 rounded border border-gray-400 w-full appearance-none" disabled />
            @else
              <x-input type="file" name="image" wire:model="image"
                class="p-2 rounded border border-gray-400 w-full appearance-none" />
            @endif
          </x-input-group>

          {{-- ATTRIBUTION --}}

          <x-input-group name="attribution">
            @if ($disabled)
              <x-textarea name="attribution" wire:model="attribution"
                class="p-2 rounded border border-gray-400 w-full appearance-none" rows="2" disabled />
            @else
              <x-textarea name="attribution" wire:model="attribution"
                class="p-2 rounded border border-gray-400 w-full appearance-none" rows="2" />
            @endif
          </x-input-group>

        </div>

        {{-- TAB 2 --}}

        <div x-show="openTab === 2">

          {{-- EMBEDDED IMAGE OR VIDEO --}}

          <x-input-group name="embedded_image_or_video">
            @if ($disabled)
              <x-textarea name="embed" wire:model="embed"
                class="p-2 rounded border border-gray-400 w-full appearance-none" rows="4" disabled />
            @else
              <x-textarea name="embed" wire:model="embed"
                class="p-2 rounded border border-gray-400 w-full appearance-none" rows="4" />
            @endif
          </x-input-group>

          <div class="font-nunito-light text-xs text-gray-700 -mt-2 mb-2">
            For embedded videos, replace height and width with class="absolute top-0 left-0 w-full h-full"
          </div>

          {{-- EMBED ON SMALL SCREENS --}}

          <div class="pb-3">
            <div class="relative inline-block w-12 mr-2 align-middle select-none transition duration-200 ease-in">
              <input type="checkbox" name="embed_on_small_screens" id="embed_on_small_screens"
                wire:model="embed_on_small_screens"
                class="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 border-red-600 appearance-none cursor-pointer" />
              <label for="embed_on_small_screens"
                class="toggle-label block overflow-hidden h-6 rounded-full bg-red-600 cursor-pointer"></label>
            </div>
            <label for="embed_on_small_screens"
              class="{{ $embed_on_small_screens ? 'text-green-600' : 'text-red-600' }} uppercase pr-4">
              {{ $embed_on_small_screens ? 'Embed on Small Screens' : "Don't Embed on Small Screens" }}
            </label>
          </div>

          {{-- ATTRIBUTION --}}

          <x-input-group name="attribution">
            @if ($disabled)
              <x-textarea name="attribution" wire:model="attribution"
                class="p-2 rounded border border-gray-400 w-full appearance-none" rows="2" disabled />
            @else
              <x-textarea name="attribution" wire:model="attribution"
                class="p-2 rounded border border-gray-400 w-full appearance-none" rows="2" />
            @endif
          </x-input-group>

        </div>
      </div>
    </div>

    {{-- IMAGE DISPLAY --}}

    @if ($showImage === true && !is_null($previousImage))
      <div class="w-1/3">
        <img src={{ asset('storage/postimages/' . $previousImage) }} />
      </div>
    @endif

  </div>

  {{-- SECOND COLUMN --}}

  <div class="col-span-4">

    {{-- TITLE --}}

    <x-input-group name="title">
      @if ($disabled)
        <x-input name="title" wire:model="title" class="p-2 rounded border border-gray-400 w-full appearance-none"
          disabled />
      @else
        <x-input name="title" wire:model="title" class="p-2 rounded border border-gray-400 w-full appearance-none" />
      @endif
    </x-input-group>

    {{-- AUTHOR --}}

    <x-input-group name="author">
      @if ($disabled)
        <x-input name="author" wire:model="author" class="p-2 rounded border border-gray-400 w-full appearance-none"
          disabled />
      @else
        <x-input name="author" wire:model="author" class="p-2 rounded border border-gray-400 w-full appearance-none" />
      @endif
    </x-input-group>

    {{-- SOURCE --}}

    <x-input-group name="source">
      @if ($disabled)
        <x-textarea name="source" wire:model="source" rows="3"
          class="p-2 rounded border border-gray-400 w-full appearance-none" disabled />
      @else
        <x-source wire:model.lazy="source" id="source" :initial-value="$source" />
      @endif
    </x-input-group>

    {{-- LINK --}}

    <x-input-group name="link">
      @if ($disabled)
        <x-input name="link" wire:model="link" class="p-2 rounded border border-gray-400 w-full appearance-none"
          disabled />
      @else
        <x-input name="link" wire:model="link" class="p-2 rounded border border-gray-400 w-full appearance-none" />
      @endif
    </x-input-group>

    {{-- TYPE, YEAR AND SKIP --}}

    <div class="grid grid-cols-4 gap-x-4">

      {{-- TYPE --}}

      {{-- TINY INT - 127 MAX VALUE --}}

      <div class="col-span-2">
        <x-input-group name="type">
          <select name="type" id="type" wire:model="type"
            class="p-2 rounded border border-gray-400 w-full appearance-none" {{ $disabled }}>
            <option value="">Pick a type...</option>
            @if ($types)
              @foreach ($types as $key => $value)
                <option value={{ $key }}>{{ $value }}</option>
              @endforeach
            @endif
          </select>
        </x-input-group>
      </div>

      {{-- YEAR --}}

      <div class="col-span-1">
        <x-input-group name="year">
          @if ($disabled)
            <x-input name="year" wire:model="year" class="p-2 rounded border border-gray-400 w-full appearance-none"
              disabled />
          @else
            <x-input name="year" wire:model="year" class="p-2 rounded border border-gray-400 w-full appearance-none" />
          @endif
        </x-input-group>
      </div>

      {{-- SKIP --}}

      <div class="col-span-1">
        <x-input-group name="skip">
          @if ($disabled)
            <x-input name="skip" wire:model="skip" class="p-2 rounded border border-gray-400 w-full appearance-none"
              disabled />
          @else
            <x-input name="skip" wire:model="skip" class="p-2 rounded border border-gray-400 w-full appearance-none" />
          @endif
        </x-input-group>
      </div>

    </div>

  </div>

  {{-- THIRD COLUMN --}}

  <div class="col-span-4">

    {{-- CONTENT BODY TABS --}}

    <div x-data="{
      openTab: 1,
      activeClasses: 'border-l border-t border-r rounded-t border-gray-400',
      inactiveClasses: 'hover:text-tan'
    }" class="">
      <ul class="flex border-b border-gray-400 text-sm text-gray-700">
        <li @click="openTab = 1" :class="{ '-mb-px': openTab === 1 }" class="-mb-px mr-1">
          <a :class="openTab === 1 ? activeClasses : inactiveClasses" class="bg-white inline-block py-2 px-4" href=" #">
            TEXT
          </a>
        </li>
        <li @click="openTab = 2" :class="{ '-mb-px': openTab === 2 }" class="mr-1">
          <a :class="openTab === 2 ? activeClasses : inactiveClasses" class="bg-white inline-block py-2 px-4" href="#">
            EMBEDDED CONTENT
          </a>
        </li>
      </ul>
      <div class="w-full pt-4">

        {{-- TAB 1 --}}

        <div x-show="openTab === 1">

          {{-- BODY - TRIX --}}

          <x-input-group name="body">
            @if ($disabled)
              <x-textarea name="body" wire:model="body"
                class="p-2 rounded border border-gray-400 w-full appearance-none" row="10" disabled />
            @else
              <x-body wire:model.lazy="body" id="body" :initial-value="$body" />
            @endif
          </x-input-group>
        </div>

        {{-- TAB 2 --}}

        <div x-show="openTab === 2">

          {{-- BODY - TEXTAREA --}}

          <x-input-group name="body">
            @if ($disabled)
              <x-textarea name="body" wire:model="body"
                class="p-2 rounded border border-gray-400 w-full appearance-none" rows="2" disabled />
            @else
              <x-textarea name="body" wire:model="body"
                class="p-2 rounded border border-gray-400 w-full appearance-none" rows="2" />
            @endif
          </x-input-group>
        </div>
      </div>
    </div>

  </div>

</div>
