    <div class="bg-gray-50 mb-24">
      <div class="bg-white border border-gray-200 py-8 px-8">
        <h2 class=" font-roboto_bold text-4xl text-gray-700 text-center mb-4">
          {{ $entry->title }}
        </h2>

        @if ($entry->formattedPubishedDate)
          <div class="font-nunito_light text-xs text-gray-500 text-center">
            {{ $entry->formattedPubishedDate }}
          </div>
        @endif
      </div>

      <div class="w-full bg-white lg:grid grid-cols-2 gap-x-8 border-l border-r border-b border-gray-200 py-8 px-8">

        <div class="col-span-1 pt-2 pb-4">
          @if ($entry->image)
            <div class="">
              <img src={{ asset('storage/postimages/' . $entry->image) }} />
            </div>
          @endif

          @if ($entry->embed)
            <div class="{{ $entry->embed_on_small_screens ? '' : 'hidden lg:block' }} relative h-0 pb-fluid-video">
              {!! $entry->embed !!}
            </div>
          @endif
        </div>

        <div class="col-span-1 {{ $source === 'backend' ? 'border-b border-red-700' : '' }}">

          @if ($entry->body)
            <div class="text-lg font-nunito_semibold text-gray-700 text-justify mb-4">
              {!! $entry->body !!}
            </div>
          @endif


          <div class=" text-gray-600 text-base font-nunito_light">
            @if ($entry->author)
              <div class="">
                {{ $entry->author }}
              </div>
            @endif

            @if ($entry->source)
              <div class="">
                {!! $entry->source !!}
              </div>
            @endif

            @if ($entry->link)
              <div>
                <a href="{{ $entry->link }}" target="_blank" rel="noopener noreferrer">
                  <span class="underline hidden md:block">{{ $entry->link }}</span>
                  <span class="underline md:hidden">Link</span>
                </a>
              </div>
            @endif

            @if ($entry->attribution)
              <div class="">
                {!! $entry->attribution !!}
              </div>
            @endif

            @if ($entry->year)
              <div class="">
                {{ $entry->year }}
              </div>
            @endif

            <div class="hidden lg:block">
              @for ($i = 0; $i < $entry->skip; $i++) <br> @endfor
            </div>

          </div>

        </div>

      </div>

    </div>
