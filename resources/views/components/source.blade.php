@props(['initialValue' => ''])

<div wire:ignore {{ $attributes }} x-data @trix-blur="$dispatch('change', $event.target.value)">

  <input id="y" value="{{ $initialValue }}" type="hidden">
  <trix-editor input="y"></trix-editor>
</div>
