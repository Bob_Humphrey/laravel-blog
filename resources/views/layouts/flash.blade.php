<x-alert type="success" class="w-full bg-green-700 text-white text-center p-4 mx-auto" />
<x-alert type="warning" class="w-full bg-yellow-700 text-white text-center p-4 mx-auto" />
<x-alert type="danger" class="w-full bg-red-700 text-white text-center p-4 mx-auto" />
