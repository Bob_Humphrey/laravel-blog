@php
use Illuminate\Support\Str;
$path = request()->path();
$frontend = Str::of($path)->exactly('/') || Str::of($path)->contains('blog/') ? true : false;
@endphp

<x-layouts.base>

  {{-- @env('local')
  @include('layouts.screen-width')
  @endenv --}}

  @if ($frontend)

    <div class="font-nunito_regular">
      <div class="w-full md:w-full text-4xl md:text-7xl font-roboto_bold text-red-800 text-center py-12">
        <a href="/">
          Scrapbook
        </a>
      </div>

    @else

      <div>
        <nav class="bg-gray-50 py-2" x-data="{ showMenu: false }">
          <div class="flex flex-col xl:flex-row xl:justify-between xl:items-center w-11/12 mx-auto py-3">
            <div class="flex justify-between items-center">
              <div class="flex xl:justify-start"> <a href="/" class="flex flex-col xl:flex-row w-full">
                  <h1 class="font-roboto_bold text-red-800 hover:text-black text-3xl">
                    {{ config('app.name', 'Laravel Application') }}
                  </h1>
                </a> </div>
              <div class="xl:hidden w-8 cursor-pointer" @click="showMenu=!showMenu" x-show="!showMenu">
                <x-zondicon-menu />
              </div>
              <div class="xl:hidden w-8 cursor-pointer" @click="showMenu=!showMenu" x-show="showMenu">
                <x-zondicon-close />
              </div>
            </div>
            <div
              class="hidden xl:flex flex-row justify-end items-center text-lg font-inter_light xl:text-xl text-gray-700 leading-none pb-8 xl:pb-0">
              @include('layouts.mainmenu') </div>
            <div
              class="flex flex-col xl:hidden text-lg font-inter_light xl:text-xl text-gray-700 leading-none pb-8 xl:pb-0"
              x-show="showMenu"> @include('layouts.mainmenu') </div>
          </div>
        </nav>

  @endif

  @include('layouts.flash')

  <main class="p-6 w-full mx-auto">

    {{ $slot }}

  </main>

  @include('layouts.footer')
  </div>
</x-layouts.base>
