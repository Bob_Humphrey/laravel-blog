@php
use Illuminate\Support\Str;
$path = request()->path();
$admin = Str::of($path)->exactly('admin') ? true : false;
$add = Str::of($path)->exactly('add') ? true : false;
$future = Str::of($path)->exactly('future') ? true : false;
$published = Str::of($path)->exactly('published') ? true : false;
$unpublished = Str::of($path)->exactly('unpublished') ? true : false;
$home = Str::of($path)->exactly('/') ? true : false;
@endphp

<a href="{{ url('/add') }}" class="{{ $add ? 'text-red-800' : '' }} link lg:px-2 py-2">
  Add
</a>
<a href="{{ url('/future') }}" class="{{ $future ? 'text-red-800' : '' }} link lg:px-2 py-2">
  Future
</a>
<a href="{{ url('/published') }}" class="{{ $published ? 'text-red-800' : '' }} link lg:px-2 py-2">
  Published
</a>
<a href="{{ url('/unpublished') }}" class="{{ $unpublished || $admin ? 'text-red-800' : '' }} link lg:px-2 py-2">
  Unpublished
</a>

@guest
  <a href="{{ route('login') }}" class="link lg:px-2 py-2">
    Login
  </a>
@endguest

@auth
  <a href="{{ url('/logout') }}" class="link lg:px-2 py-2">
    Logout
  </a>

  <div class="lg:ml-4">
    <livewire:search-bar />
  </div>
@endauth

{{-- @php
$user = Auth::user();
@endphp
<div>
  @if ($user)
    {{ $user->name }}
  @endif
</div> --}}
