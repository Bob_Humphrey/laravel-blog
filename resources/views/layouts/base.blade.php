<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description"
    content="The personal blog of Bob Humphrey, built using the TALL Stack - Tailwind CSS, Alpine JS, Laravel and Livewire.">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->

  <!-- Styles -->
  @livewireStyles
  @bukStyles(true)
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="bg-gray-50">

  {{ $slot }}

  <!-- Scripts -->
  @livewireScripts
  @bukScripts(true)
  <script src="{{ asset('js/app.js') }}" defer></script>

</body>

</html>
